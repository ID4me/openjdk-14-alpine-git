FROM openjdk:14-alpine


RUN echo "Updating data of package repositories ..." && \
    apk update --no-cache && \
    echo "Installing package 'git' ..." && \
    apk add git && \
    apk add nodejs npm
